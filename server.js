var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var session = require('express-session');

//Moteur de template

app.set('view engine', 'ejs');

//Middleware

app.use('/assets', express.static('public'));

app.use(bodyParser.urlencoded({ extended: false }));

app.use(bodyParser.json());

app.use(session({
    secret: 'aazeazeeaz',
    resave: false,
    saveUninitialized: true,
    cookie: { secure: false }
}));

app.use(require('./middlewares/flash'));

//Routes

app.get('/', (request, response) => {
    var Message = require('./models/message');
    Message.all(function(messages) {
        response.render('pages/index', {messages: messages})
});

});

app.post('/', (request, response) => {
    if (request.body.message === undefined || request.body.message === '') {
        request.flash('error', "Vous n'avez pas posté de message");
        response.redirect('/');
}   else {
    var Message = require('./models/message');
    Message.create(request.body.message, function() {
        request.flash('success', "Merci !");
        response.redirect('/');
    });
}

});

app.get('/message/:id', (request, response) => {
    var Message = require('./models/message');
    Message.find(request.params.id, function(message) {
        response.render('messages/show', {message: message});
});
});

app.listen(8080);